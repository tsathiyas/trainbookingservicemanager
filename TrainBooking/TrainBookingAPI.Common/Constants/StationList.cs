﻿using System.Collections.Generic;

namespace TrainBookingAPI.Common
{
    public class StationList
    {
        public static readonly List<string> StationNames = new List<string>
            {
                "DARTFORD",
                "DARTMOUTH",
                "TOWER HILL",
                "DERBY",
                "LIVERPOOL",
                "LIVERPOOL LIME STREET",
                "PADDINGTON",
                "EUSTON",
                "LONDON BRIDGE",
                "VICTORIA"
            };
    }
}
