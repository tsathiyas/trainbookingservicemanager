﻿using System.Collections.Generic;

namespace TrainBookingAPI.Common
{
    public class ResponseModel
    {
        public List<string> StationNames;
        public List<char> NextMatchedStrings;
    }
}
