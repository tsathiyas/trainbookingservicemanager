using System.Collections.Generic;
using Xunit;

using TrainBookingAPI.Common;
using TrainBookingAPI.ServiceInterfaces;
using TrainBookingAPI.Services;
using System;
using Moq;

namespace TrainBookingAPI.Tests
{
    public class GetStationsBySearchKeyWordTests
    {
        IGetStationNamesBySearchKeyWord _getStationNamesBySearchKeyWord;
        private IGetResultSet _getMatchedStationNamesNextCharacters;

        public GetStationsBySearchKeyWordTests()
        {
            _getMatchedStationNamesNextCharacters = new GetResultSet();
            _getStationNamesBySearchKeyWord = new GetStationNamesBySearchKeyWord(_getMatchedStationNamesNextCharacters);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Return_Empty_Given_Value_As_Null_Or_Empty(string searchKeyWord)
        {
            //Act
            ResponseModel responseModel = _getStationNamesBySearchKeyWord.GetStationNames(searchKeyWord);

            //Assert
            Equals(0, responseModel.NextMatchedStrings.Count);
            Equals(0, responseModel.StationNames.Count);
        }

        [Fact]
        public void Return_Valid_Given_SearchKeyWord_As_LIVERPOOL()
        {
            //Arrange
            string searchKeyWord = "LIVERPOOL";
            ResponseModel expectedResponseModel = new ResponseModel
            {
                NextMatchedStrings = new List<char> { ' ' },
                StationNames = new List<string> { "LIVERPOOL", "LIVERPOOL LIME STREET" }
            };

            //Act
            ResponseModel actualResponseModel = _getStationNamesBySearchKeyWord.GetStationNames(searchKeyWord);

            //Assert
            Assert.Equal(expectedResponseModel.NextMatchedStrings, actualResponseModel.NextMatchedStrings);
            Assert.Equal(expectedResponseModel.StationNames, actualResponseModel.StationNames);
        }

        [Fact]
        public void Return_Space_Given_SearchKeyWord_As_LIVERPOOL()
        {
            //Arrange
            string searchKeyWord = "LIVERPOOL";
            ResponseModel expectedResponseModel = new ResponseModel
            {
                NextMatchedStrings = new List<char> { ' ' },
                StationNames = new List<string> { "LIVERPOOL", "LIVERPOOL LIME STREET" }
                };

            //Act
            ResponseModel actualResponseModel = _getStationNamesBySearchKeyWord.GetStationNames(searchKeyWord);

            //Assert
            Assert.Equal(expectedResponseModel.NextMatchedStrings, actualResponseModel.NextMatchedStrings);
            Assert.Equal(expectedResponseModel.StationNames, actualResponseModel.StationNames);
        }

        [Fact]
        public void Return_Empty_Given_Invalid_SearchKeyWord_As_Test()
        {
            //Arrange
            string searchKeyWord = "Test";

            //Act
            ResponseModel responseModel = _getStationNamesBySearchKeyWord.GetStationNames(searchKeyWord);

            //Assert
            Assert.Empty(responseModel.NextMatchedStrings);
            Assert.Empty(responseModel.StationNames);
        }

        [Fact]
        public void Return_ValidCharacters_Given_SearchKeyWord_As_Lowercase_dart()
        {
            //Arrange
            string searchKeyWord = "dart";
            ResponseModel expectedResponseModel = new ResponseModel
            {
                NextMatchedStrings = new List<char> { 'F', 'M' },
                StationNames = new List<string> { "DARTFORD", "DARTMOUTH" }
            };

            //Act
            ResponseModel actualResponseModel = _getStationNamesBySearchKeyWord.GetStationNames(searchKeyWord);

            //Assert
            Assert.Equal(expectedResponseModel.NextMatchedStrings, actualResponseModel.NextMatchedStrings);
            Assert.Equal(expectedResponseModel.StationNames, actualResponseModel.StationNames);
        }

        [Fact]
        public void LoadTest()
        {
            //Arrange
            List<string> originalDataCollection = new List<string>();
            var watch = System.Diagnostics.Stopwatch.StartNew();

            Random r = new Random(1000000);
            for (int i = 0; i < 1000000; i++)
            {
                int number = r.Next();
                string str = "Gate" + number;
                originalDataCollection.Add(str);
            }

            string searchTerm = "Gate3";
            var searchItems = _getMatchedStationNamesNextCharacters
                                .GetSearchResultSet(originalDataCollection, searchTerm);

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
        }
    }
}
