﻿using Microsoft.AspNetCore.Mvc;

using TrainBookingAPI.Common;
using TrainBookingAPI.ServiceInterfaces;

namespace TrainBookingAPI.Controllers
{
    [Route("stations/[controller]")]
    public class SearchController : Controller
    {
        private readonly IGetStationNamesBySearchKeyWord _getStationNames;

        public SearchController(IGetStationNamesBySearchKeyWord getStationNames)
        {
            this._getStationNames = getStationNames;
        }

        [Produces("application/json")]
        [HttpGet("{searchKeyWord}")]
        public JsonResult GetStationNames(string searchKeyWord)
        {
            ResponseModel responseModel = _getStationNames.GetStationNames(searchKeyWord);

            return new JsonResult(responseModel);
        }
    }
}
