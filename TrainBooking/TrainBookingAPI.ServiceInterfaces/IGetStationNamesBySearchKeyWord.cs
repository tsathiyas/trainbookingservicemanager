﻿using TrainBookingAPI.Common;

namespace TrainBookingAPI.ServiceInterfaces
{
    public interface IGetStationNamesBySearchKeyWord
    {
        ResponseModel GetStationNames(string searchKeyWord);
    }
}
