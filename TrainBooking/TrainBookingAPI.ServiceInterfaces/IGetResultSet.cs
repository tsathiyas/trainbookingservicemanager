﻿using System.Collections.Generic;

namespace TrainBookingAPI.ServiceInterfaces
{
    public interface IGetResultSet
    {
        (List<string>, List<char>) GetSearchResultSet(List<string> matchedStationNames, string searchKeyWord);
    }
}
