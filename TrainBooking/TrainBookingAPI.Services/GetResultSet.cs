﻿using System.Collections.Generic;
using System.Linq;
using TrainBookingAPI.ServiceInterfaces;

namespace TrainBookingAPI.Services
{
    public class GetResultSet : IGetResultSet
    {
        public (List<string>, List<char>) GetSearchResultSet(List<string> stationNames, string searchKeyWord)
        {
            List<char> nextChars = new List<char>();
            List<string> matchingStationNames = new List<string>();

            if (searchKeyWord != null)
            {
                stationNames.AsParallel().ForAll(x =>
                {
                    if (x.ToLower().StartsWith(searchKeyWord.ToLower()))
                    {
                        matchingStationNames.Add(x);
                        GetMatchingCharacters(searchKeyWord, x, nextChars);
                    }
                });
            }

            return (matchingStationNames, nextChars);
        }

        private void GetMatchingCharacters(string searchKeyWord, string stationName, List<char> nextChars)
        {
            if (stationName.Length > searchKeyWord.Length)
            {
                char nextChar = stationName[searchKeyWord.Length];
                if (!nextChars.Exists(y => y.Equals(nextChar)))
                {
                    nextChars.Add(nextChar);
                }
            }
        }
    }
}
