﻿using System.Collections.Generic;
using TrainBookingAPI.Common;
using TrainBookingAPI.ServiceInterfaces;

namespace TrainBookingAPI.Services
{
    public class GetStationNamesBySearchKeyWord : IGetStationNamesBySearchKeyWord
    {
        private IGetResultSet _getResultSet;

        public GetStationNamesBySearchKeyWord(IGetResultSet getResultSet)
        {
            _getResultSet = getResultSet;
        }

        public ResponseModel GetStationNames(string searchKeyWord)
        {
            ResponseModel responseModel = new ResponseModel
            {
                StationNames = new List<string>(),
                NextMatchedStrings = new List<char>()
            };
            
            var response = _getResultSet.GetSearchResultSet(StationList.StationNames, searchKeyWord);
            responseModel.StationNames = response.Item1;
            responseModel.NextMatchedStrings = response.Item2;

            return responseModel;
        }
    }
}
